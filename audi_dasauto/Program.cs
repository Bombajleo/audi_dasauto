﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudiCarShowRoom
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("##### WSZYSTKIE DOSTĘPNE AUTA AUDI ####");
            ShowModelss audi = new ShowModelss("Audi", 0, 100);
            audi.AddModels("A1  ", 0, 39000);
            audi.AddModels("A2  ", 1, 41000);
            audi.AddModels("A3  ", 2, 45000);
            audi.AddModels("A4  ", 3, 50000);
            audi.AddModels("A5  ", 4, 53000);
            audi.AddModels("A6  ", 5, 55000);
            audi.AddModels("A7  ", 6, 70000);
            audi.AddModels("S3  ", 7, 100000);
            audi.AddModels("S4  ", 8, 110000);
            audi.AddModels("S5  ", 9, 115000);
            audi.AddModels("S6  ", 10, 119000);
            audi.AddModels("S7  ", 11, 123000);
            audi.AddModels("RS3  ", 12, 150000);
            audi.AddModels("RS4  ", 13, 170000);
            audi.AddModels("RS5  ", 14, 200000);
            audi.AddModels("RS6  ", 15, 450000);
            audi.AddModels("RS7  ", 16, 500000);
            audi.AddModels("R8  ", 17, 900000);



            Console.WriteLine("#### DOSTĘPNE KOLORY #### ");
            Console.WriteLine("#### KOLOR JEST W CENIE PODSTAWOWEJ AUTA #### ");

            audi.AddCarColor("Biały", 30);
            audi.AddCarColor("Czarny", 31);
            audi.AddCarColor("Żółty", 32);
            audi.AddCarColor("Srebrny", 33);
            audi.AddCarColor("Czerwony", 34);
            audi.AddCarColor("Mat", 35);
            audi.AddCarColor("Pomarańczowy", 36);
            audi.AddCarColor("Niebieski", 37);
            audi.AddCarColor("Zielony", 38);
            audi.AddCarColor("Kobaltowy", 39);


            Console.WriteLine("#### DOSTĘPNE SILNIKI : #####");

            audi.AddEngine("1.6b + Turbo", "120Kw", 5000, 100);
            audi.AddEngine("1.8b + Turbo", "140Kw", 6300, 101);
            audi.AddEngine("1.9Tdi", "100Kw", 3100, 102);
            audi.AddEngine("2.0 + Turbo", "150Kw", 4000, 103);
            audi.AddEngine("2.0Tdi", "90Kw", 3900, 104);
            audi.AddEngine("2.5tfsi + Turbo", "120Kw", 5000, 105);
            audi.AddEngine("2.8Tdi + Turbo", "120Kw", 6100, 106);
            audi.AddEngine("3.0 +BiTurbo", "120Kw", 8000, 107);
            audi.AddEngine("3.0Tdi", "120Kw", 6900, 108);
            audi.AddEngine("4.0 + Turbo", "120Kw", 16000, 109);
            audi.AddEngine("4.2 b", "120Kw", 13000, 110);
            audi.AddEngine("4.2 + BiTurbo", "120Kw", 20000, 111);
            audi.AddEngine("5.0 TFSI", "120Kw", 30000, 112);
            audi.AddEngine("5.2TFSI", "120Kw", 31000, 113);

            Console.WriteLine("##### SKRZYNIE BIEGÓW: ####");

            audi.GetTransmission("Automatyczna typu TIP TRONIC", 10000, 200);
            audi.GetTransmission("Automatyczna typu S TRONIC", 25000, 201);
            audi.GetTransmission("MANUALNA", 7000, 202);


            Console.WriteLine("$$$$$ WYPOSAŻENIE DO EXTRA WYKUPU: $$$$$");
            audi.AddEqu("Navigacja", 999, 50);
            audi.AddEqu("Fotele Kubełkowe", 2500, 51);
            audi.AddEqu("Fotele Skórzane", 2199, 52);
            audi.AddEqu("Sportowa Kierownica", 899, 53);
            audi.AddEqu("Czujniki Parkowania", 499, 54);
            audi.AddEqu("Kamera Cofania", 1299, 55);
            audi.AddEqu("Asystent Parkowania", 1999, 56);
            audi.AddEqu("Wewnętrzne Lusterko Przecioślepieniowe", 399, 57);
            audi.AddEqu("Klimatyzacja", 1699, 58);
            audi.AddEqu("Elektryczne Szyby", 299, 59);
            audi.AddEqu("Wspomaganie unoszenia bagażnika", 999, 60);
            audi.AddEqu("HAK", 679, 61);
            audi.AddEqu("Pakiet dla palących", 399, 62);
            audi.AddEqu("System BOSE", 2999, 63);
            audi.AddEqu("Złącze Bluetooth", 999, 64);
            audi.AddEqu("Audi PHONE BoX", 1299, 65);
            audi.AddEqu("Parking System", 2999, 66);
            audi.AddEqu("Tempomat", 699, 67);
            audi.AddEqu("Podgrzewane fotele", 999, 68);
            audi.AddEqu("Podgrzewana kierownica", 299, 69);
            audi.AddEqu("Rozszerzona gwarancja", 3999, 70);
            







            audi.AddOrder("Model : ", 0);












            Console.Read();

        }
    }
}
