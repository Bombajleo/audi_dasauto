﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudiCarShowRoom
{
   public class Transmission
    {
        public string Name;
        public int Id;
        public int Price;

        public Transmission(string name, int id, int price)
        {
            this.Name = name;
            this.Id = id;
            this.Price = price;
        }

    }
}
